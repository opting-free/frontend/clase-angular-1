export class Pet {
  public id: number;
  public name: string;
  public color: string;

  constructor(data: any = null) {
    this.id = data.id;
    this.name = data.name;
    this.color = data.color;
  }
}