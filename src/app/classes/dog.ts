import { Pet } from './pet';

export class Dog extends Pet {
  public raza: string;
  public isMale: boolean;

  constructor(data: any = null) {
    super(data);
    this.raza = data.raza;
    this.isMale = data.isMale;
  }
}