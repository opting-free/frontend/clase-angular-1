import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public counter: number = 0;
  public word: string;

  constructor() { }

  public ngOnInit(): void {
  }

  public sumarUno(): void {
    this.counter++;
  }
}
