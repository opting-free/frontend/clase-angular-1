import { Component, OnInit } from '@angular/core';
import { AnimalsService } from 'src/app/services/animals.service';
import { Cat } from 'src/app/classes/cat';

@Component({
  selector: 'app-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.scss']
})
export class CatsComponent implements OnInit {
  public cats: Cat[];

  constructor(private animalService: AnimalsService) { }

  public ngOnInit(): void {
    this.animalService.getCats().then(data => {
      this.cats = data;
    })
  }
}
