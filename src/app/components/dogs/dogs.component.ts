import { Component, OnInit } from '@angular/core';
import { AnimalsService } from 'src/app/services/animals.service';
import { Dog } from 'src/app/classes/dog';

@Component({
  selector: 'app-dogs',
  templateUrl: './dogs.component.html',
  styleUrls: ['./dogs.component.scss']
})
export class DogsComponent implements OnInit {
  public dogs: Dog[];
  public show: boolean = false

  constructor(private animalsService: AnimalsService) { }

  ngOnInit(): void {
    this.animalsService.getDogs().then((data: Dog[]) => {
      this.dogs = data;
    })
  }

  public toggleDogs(): void {
    this.show = !this.show;
  }
}
