import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { scheduled } from 'rxjs';
scheduled
@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {
  @Input('show') public isShowing: boolean = true;
  @Output() public toggleTable: EventEmitter<any> = new EventEmitter();

  constructor() { }

  public ngOnInit(): void {
  }

  public toggleDogs(): void {
    this.toggleTable.emit()
  }
}
