import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'Stream';

  constructor(public route: Router) {

  }

  public goToHome(): void {
    this.route.navigate(['']);
  }

  public goToDogs(): void {
    this.route.navigate(['dogs']);
  }

  public goToCats(): void {
    this.route.navigate(['cats']);
  }

  public goToAbout(): void {
    this.route.navigate(['about']);
  }
}
