import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cat } from '../classes/cat';
import { Dog } from '../classes/dog';

@Injectable({
  providedIn: 'root'
})
export class AnimalsService {

  constructor(private http: HttpClient) { }

  public async getCats(): Promise<Cat[]> {
    return new Promise<Cat[]>((resolve, reject) => {
      this.http.get('../../assets/cats.json').subscribe((data: any) => {
        const cats = data.map(cat => new Cat(cat));
        resolve(cats);
      }, error => {
        reject(error)
      })
    })
  }

  public async getDogs(): Promise<Dog[]> {
    return new Promise<Dog[]>((resolve, reject) => {
      this.http.get('../../assets/dogs.json').subscribe((data: any) => {
        const dogs = data.map(cat => new Dog(cat));
        resolve(dogs);
      })
    })
  }
}
